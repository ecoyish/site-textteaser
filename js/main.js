$(document).on('ready', function() {

    $('#placeholder').click(function() {
        $(this).next('.form-control').focus();
        $('#inputContainer').addClass('active');
    });

    var cur_year = new Date().getFullYear();
    $('#currentYear').html(cur_year);

    // Rotating text
    var pos = 1;
    var limit = 3;
    var cur_pos = pos;
    setInterval(
        function(){ 
            if (cur_pos == limit) {
                cur_pos = pos;
                $('*[data-position="'+ cur_pos +'"]').addClass('active flipInX').removeClass('flipOutX');
                $('*[data-position="'+ limit +'"]').addClass('fadeOut').removeClass('active flipInX');
            }
            else {
                cur_pos = cur_pos + 1;
                $('*[data-position="'+ cur_pos +'"]').addClass('active flipInX').removeClass('fadeOut');
                $('*[data-position="'+ (cur_pos - 1) +'"]').removeClass('active flipInX').addClass('fadeOut');
            }
        }
        , 6000
    );

});
